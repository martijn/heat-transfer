SIZE=$1

function programExists() {
  which $1 2>/dev/null >> /dev/null
}

function convertBinaryFramesToPNG() {

  # Put all the arguments to GNU parallel in a file, because there might be too
  # many arguments for providing them on the command line
  TMPFILE=$(mktemp)
  find frames/ -type f -iname "*.bin" > ${TMPFILE}

  # Convert each .bin file to a .png file with imagemagick. The .bin files are
  # just blobs without metadata. We have to tell imagemagick the size and that
  # they are 8bit grayscale.
  echo -n "Converting the frames to png... "
  CMD="echo \"processing {}\"; magick convert -size ${SIZE} -depth 8 gray:{} frames/{/.}.png"
  parallel -j8 "${CMD}" < "${TMPFILE}"
  echo "done."

  # Clean up
  rm $TMPFILE
  # Optionally remove the .bin files
  # rm frames/*.bin
}

function makeVideoFromPNG() {
  echo "making video"
  ffmpeg -r 30 -f image2 -s "${SIZE}" -i frames/frame-%06d.png \
    -vcodec libx264 -crf 15  -pix_fmt yuv420p test.mp4

  # Clean up
  # Optionally remove the .png files
  # rm frames/*.png
}

function main() {
  convertBinaryFramesToPNG
  makeVideoFromPNG
  exit 0
}

case "${#}" in
  1)
    if programExists ffmpeg && programExists parallel && programExists magick; then
      main $1
    else
      echo -e "To run this script you need all of the following packages:\n\n * GNU parallel\n * ffmpeg\n * magick"
    fi
    ;;
  *)
    echo "First argument is the size, example: binaryDataToVideo 100x100"
    ;;
esac

