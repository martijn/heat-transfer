{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE FlexibleContexts      #-}

module Main where

import           Data.Array.Accelerate ((:.)(..), Exp, Acc, Z(..), DIM2)
import qualified Data.Array.Accelerate             as A
-- import qualified Data.Array.Accelerate.Interpreter as I
import qualified Data.Array.Accelerate.LLVM.Native as I
--import qualified Data.Array.Accelerate.Debug       as D

type Length      = Int
type Temperature = Double
type MyMatrix ix elem = A.Array (Z :. ix :. ix) elem
type Beam        = MyMatrix Length Temperature

-- Problem setup
ambientT, hotT :: Temperature
ambientT = 293.15
hotT     = ambientT + 100

w_, h_ :: Length
w_ = 1000
h_ = 1000

iterations :: Int
iterations = 10000

scalefactor :: Exp Temperature
scalefactor =
  A.constant (
    product
      [ 1.00e-6 -- Area
      , 4.01e+2 -- conduction coefficient for copper
      , 1.00e+4 -- dT, time step
      , 1.00e-3 -- go from seconds to milliseconds
      , 1.00e+2 -- artificial scaling constant
      ]
    )

main :: IO ()
main =
  let sh   = (Z :. w_ :. h_) :: (Z :. Length :. Length)
      arr  = A.fromList sh (repeat ambientT)
      arr' = I.run (simulation iterations (A.use arr))
  in do
    print arr'
    return () -- putStrLn (dump arr')

-- Accelerate stuff

isLeftBoundary ::  Exp DIM2 -> Exp Bool
isLeftBoundary ix =
  let (_ :. _ :. x) = A.unlift ix :: (Exp Z :. Exp Int :. Exp Int)
   in x A.< A.constant 0

stepA :: Acc Beam -> Acc Beam
stepA = A.stencil ss sb
  where
    -- dUdX_dUdY, we only consider horizontal and vertical heat flow
    ss :: A.Stencil3x3 Temperature -> Exp Temperature
    ss (( _, b, _)
      , ( d, e, f)
      , ( _, h, _)
      ) = let dUdX_dUdY = (b + h - 2 * e) + (d + f - 2 * e)
           in e + scalefactor * dUdX_dUdY

    -- Boundary condition: If we are at the left boundary then we want to have a
    -- constant `hotT` and if we are elsewhere we will take `ambientT`
    sb :: A.Boundary Beam
    sb = A.function (\i -> A.cond (isLeftBoundary i)
                                  (A.constant hotT)
                                  (A.constant ambientT))

-- Runs the stepA for a set number of times
--simulation :: Beam -> Acc Beam
simulation :: Int -> Acc Beam -> Acc Beam
simulation n = aFor n stepA

{- | Iterates a state transformer function for a set number of times over an
 - initial state. Common pattern in simulations:
 -
 - s = ...; // s is some state
 -
 - for (uint8_t i = 0; i < 1000; ++i) {
 -   s = f(s); // state is transformed
 - }
 -
 - This pattern can be expressed with
 - aFor 1000 f s
 -}
aFor
  :: A.Arrays a
  => Int              -- ^ Amount of iterations
  -> (Acc a -> Acc a) -- ^ State transformer function
  -> Acc a            -- ^ Initial state
  -> Acc a
aFor n f s0 = A.asnd $ A.awhile cond body (A.T2 (A.unit 0) s0)
  where
    cond (A.T2 i _) = A.unit (A.the i A.< A.constant n)
    body (A.T2 i s) =
      let i' = A.unit (A.the i + 1)
       in A.T2 i' (f s)

