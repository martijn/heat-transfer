CC                := gcc
CXX               := g++
override IFLAGS   +=
override LDFLAGS  +=
override CFLAGS   += -O2 -lm -DPREFILL_BOUNDARIES -DVIDEO #-g3 -gdwarf-2 -DPREFILL_BOUNDARIES
override CXXFLAGS += -O2

HSC               := ghc
override HSFLAGS  += -O3 -g -package mtl

BUILDDIR  := build
CSRCDIR   := c
HSSRCDIR  := hs-plain
ACCSRCDIR := hs-accelerate
HLSRCDIR  := halide

all: heat-transfer-gcc heat-transfer-clang heat-transfer-ghc

$(BUILDDIR):
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/.hsfiles:
	mkdir -p $@

heat-transfer-gcc: $(CSRCDIR)/heat-transfer.c | $(BUILDDIR)
	gcc $(CFLAGS) $(IFLAGS) $(LDFLAGS) -o $(BUILDDIR)/$@ $<

heat-transfer-clang: $(CSRCDIR)/heat-transfer.c | $(BUILDDIR)
	clang $(CFLAGS) $(IFLAGS) $(LDFLAGS) -o $(BUILDDIR)/$@ $<

heat-transfer-ghc: $(HSSRCDIR)/Main.hs | $(BUILDDIR)/.hsfiles
	$(HSC) $(HSFLAGS) -o $(BUILDDIR)/$@ -odir $(BUILDDIR)/.hsfiles -hidir $(BUILDDIR)/.hsfiles $<

video:
	bash binaryDataToVideo.sh

clean:
	rm -rf $(BUILDDIR)
