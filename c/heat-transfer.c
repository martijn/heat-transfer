#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>

#ifdef VIDEO
#include <sys/stat.h>
#endif

#define UNIT_TEMP   double  // in kelvin
#define UNIT_LENGTH int32_t // in mm
#define UNIT_TIME   int32_t // in ms

#define MILLI_TO_UNIT(type, millis) (((type) (millis)) / (type) 1000)
#define MS_TO_S(t, ms) MILLI_TO_UNIT(t, ms)
#define MM_TO_M(t, mm) MILLI_TO_UNIT(t, mm)

#define FRAME_BASE_DIR "frames"
#define FRAMES_PER_SEC 30

static uint16_t frame_divisor;
static char     errmsg[256];

/**
 * Compiler -D flags:
 *
 * PREFILL_BOUNDARY  Toggle prefilling array cells of which the solution is
 *                   known in advance
 * VIDEO             Enable rendering of video
 * INTERRUPT         Interrupt execution and print the array every iteration
 */

typedef enum BoundaryCondition {
    Dirichlet  // y     = f
  , VonNeumann // dy/dn = f
} BoundaryConditionT;

static const UNIT_TEMP NIST_STP_TEMP = 293.15; // K

static const UNIT_LENGTH
  thickness = 1, // 1 mm
  mesh      = 1; // 1 mm, mesh resolution

typedef struct HeatTransfer {
  UNIT_LENGTH        width;     // width  of the plate in mm
  UNIT_LENGTH        height;    // height of the plate in mm
  UNIT_TEMP          ambientT;  // Ambient temperature of the `room', in K
  UNIT_TEMP          hotT;      // Temperature of the hot reservoir in K
  UNIT_TEMP          stepscale; // scale value = k * dt / thickness / meshsize
  BoundaryConditionT boundaryCondition;
  UNIT_TEMP *        plate;     // the plate, needs initialisation
} HeatTransferT;

static inline void
swap(void ** ptr1, void ** ptr2) {
  void * tmp;
  tmp   = *ptr1;
  *ptr1 = *ptr2;
  *ptr2 = tmp;
}

HeatTransferT *
initialisePlate(
    UNIT_LENGTH        width
  , UNIT_LENGTH        height
  , UNIT_TEMP          ambientT
  , UNIT_TEMP          dHotT
  , UNIT_TEMP          k
  , UNIT_TIME          timestep
  , BoundaryConditionT boundaryCondition) {

  /**
   * Given the initial configuration of the problem we set up the plate. This
   * allocates an array and initializes all cells in the plate to be at ambient
   * temperature.
   */

  HeatTransferT * h = (HeatTransferT *) malloc(sizeof(HeatTransferT));

  h->width    = width;
  h->height   = height;
  h->ambientT = ambientT;
  h->hotT     = h->ambientT + dHotT;
  h->boundaryCondition = boundaryCondition;

  h->stepscale = k * MS_TO_S(UNIT_TEMP, timestep)
                 / (MM_TO_M(UNIT_TEMP, thickness) * MM_TO_M(UNIT_TEMP, mesh));

  UNIT_LENGTH size = sizeof(UNIT_TEMP) * width * height;
  h->plate = (UNIT_TEMP *) malloc(size);

  for (UNIT_LENGTH i = 0; i < height * width; ++i) {
    h->plate[i] = ambientT;
  }

  // Bring 1 side (the left side) in contact with the hot reservoir
  for (UNIT_LENGTH i = 0; i < height * width; i += h->width) {
    h->plate[i] = h->hotT;
  }

  return h;
}

/**
 * Test whether an index is inside the bounds of a plate.
 */
static inline uint8_t
ixInBounds(HeatTransferT * h, UNIT_LENGTH x, UNIT_LENGTH y) {
  return (0 <= x && x < h->width) && (0 <= y && y < h->height);
}

static inline uint8_t
isBoundary(HeatTransferT * h, UNIT_LENGTH x, UNIT_LENGTH y) {
  return (x == 0 || x == h->width - 1);
}

/**
 * Safe indexing into the plate array. Returns ambient temperature outside the
 * plate bounds where x > 0 or the hot reservoir temperature where x <= 0. If the
 * index is inside the array bounds then we use striding to compute the index.
 */
UNIT_TEMP *
ix(HeatTransferT * h, UNIT_LENGTH x, UNIT_LENGTH y) {

  UNIT_TEMP * res;

  switch (h->boundaryCondition) {
    case Dirichlet:
      if (ixInBounds(h, x, y)) { // index inside the plate
        res = &h->plate[x + y * h->width];
      } else if (x >= 0) {        // Not inside the plate, but still outside the hot reservoir
        res = &h->ambientT;
      } else {                   // neither of the above, so inside the hot reservoir
        res = &h->hotT;
      }
      break;

    case VonNeumann:
      // TODO
      perror("Von Neumann boundary condition is not implemented.\n");
      exit(EXIT_FAILURE);
      break;

    default:
      snprintf(errmsg, 256, "Unknown boundary condition (enum): %d\n", h->boundaryCondition);
      perror(errmsg);
      exit(EXIT_FAILURE);
      break;
  }

  return res;
}

/**
 * Compute the partial differential at the coordinate (x, y).
 *
 * Example:
 *
 *     X -->
 *     0 1 2 . . .
 * Y 0 a b c
 * | 1 d e f
 * V 2 g h i
 *   .
 *   .
 *   .
 *
 * We'll assume net heat flow from the top and left, altough it doesn't really
 * matter, its an assumption a la Kirchhoffs current law.
 *
 * difference in ``accumulation of temperature'' on e, in the x dimension is:
 *
 * dT   = Tin - Tout
 * Tin  = Td - Te
 * Tout = Te - Tf
 * dT   = Td - Te - (Te - Tf)
 *      = Td - Te - Te + Tf
 *
 * the result will be the sum of the two, or the total heat that flows into this
 * cell in dT
 */
static inline UNIT_TEMP
dT(HeatTransferT * h, UNIT_LENGTH x, UNIT_LENGTH y) {

  UNIT_TEMP dTx = *ix(h, x - 1, y) + *ix(h, x + 1, y) - 2 * *ix(h, x, y);
  UNIT_TEMP dTy = *ix(h, x, y - 1) + *ix(h, x, y + 1) - 2 * *ix(h, x, y);

  return (dTx + dTy);
}

void
step(HeatTransferT * prev, HeatTransferT * next) {

  UNIT_TEMP res;

  for (UNIT_LENGTH y = 0; y < prev->height; ++y) {

#ifdef PREFILL_BOUNDARY
    // Force boundary conditions by pre-filling the array cells with the
    // solution
    switch (prev->boundaryCondition) {
      case Dirichlet:
        *ix(next, 0, y) = *ix(prev, 0, y);
        *ix(next, prev->width - 1, y) = *ix(prev, prev->width - 1, y);
        break;
    }

    for (UNIT_LENGTH x = 1; x < prev->width - 1; ++x) {
      // Tn = Tn-1 + c * dT
      *ix(next, x, y) = *ix(prev, x, y) + prev->stepscale * dT(prev, x, y);

      if(*ix(next, x, y) > next->hotT)
        continue;
    }
#else
    // Do no force boundary computations but instead compute them with a
    // conditional
    for (UNIT_LENGTH x = 0; x < prev->width; ++x) {
      *ix(next, x, y) = *ix(prev, x, y) +
        (isBoundary(prev, x, y) ? 0 : prev->stepscale * dT(prev, x, y));
    }
#endif
  }
}

// Write a single frame as a raw file. First we initialize a buffer for the
// frame, and then write normalized values to them. Normalisation maps ambient
// temperature to 0 and hot temperature to 255.
#ifdef VIDEO
void
writeFrame(HeatTransferT * h, int frameno) {


  char path[256];
  sprintf(path, "%s/frame-%06d.bin", FRAME_BASE_DIR, frameno);
  // printf("Writing frame %s\n", path);

  FILE * frame = fopen(path, "w+");
  uint8_t q;

  for (int y = 0; y < h->height; ++y) {
    for (int x = 0; x < h->width; ++x) {
      // Normalize the values to the interval [0..255]
      q = (uint8_t) (255 * ((h->plate[x + y * h->width] - h->ambientT) / (h->hotT - h->ambientT)));
      fwrite(&q, sizeof(uint8_t), 1, frame);
    }
  }
  fclose(frame);
}
#endif

void
printPlate(HeatTransferT * h) {
  for (int y = 0; y < h->height; ++y) {
    for (int x = 0; x < h->width; ++x) {
      printf("%3.2f\t", *ix(h, x, y) - 273.15);
    }
    printf("\n");
  }
}

// We are simulating heat transfer in a copper plate that is 1mm thick and
// touches a hot surface (infinite energy heat reservoir) on the left. For the
// ambient temperature we will take the standard temperature at atmospheric
// pressure defined by NIST (NIST_STP_TEMP)
void
simulate(UNIT_LENGTH w, UNIT_LENGTH h, UNIT_TEMP dT, UNIT_TIME dt, UNIT_TIME t, UNIT_TEMP k) {

#ifdef VIDEO
  if (access(FRAME_BASE_DIR, F_OK))
    mkdir(FRAME_BASE_DIR, 493); // 493 is 755 in octal
#endif

  UNIT_TIME iterations = t / dt;
  frame_divisor = iterations / (MS_TO_S(UNIT_TIME, t) * FRAMES_PER_SEC);

  // We cannot do in-place updates because it will screw up later computations.
  // So we have to do either copying or buffer swapping. I will choose buffer
  // swapping to save the declaration overhead
  HeatTransferT
    * b1 = initialisePlate(w, h, NIST_STP_TEMP, dT, k, dt, Dirichlet),
    * b2 = initialisePlate(w, h, NIST_STP_TEMP, dT, k, dt, Dirichlet);

  printf("=== Simulation setup ===\n");
  printf("Plate width:         %d mm\n", w);
  printf("Plate height:        %d mm\n", h);
  printf("Ambient temperature: %3.2f K\n", b1->ambientT);
  printf("Hot temperature:     %3.2f K\n", b1->hotT);
  printf("Time step:           %d ms\n", dt);
  printf("Simulation time:     %d  s\n", MS_TO_S(UNIT_TIME, t));
  printf("Iterations:          %d\n", iterations);
  printf("Iterations per frame:%d\n", frame_divisor);

  int framenum = 0;
  for (int i = 0; i < iterations; ++i) {
#ifdef VIDEO
    if (i % frame_divisor == 0) {
      writeFrame(b1, framenum++);
    }
#endif

#ifdef INTERRUPT
    printPlate(b1);
    getchar();
#endif

    step(b1, b2);
    swap((void **) &b1, (void **) &b2); // Swap the buffers here

  }
#ifndef NDEBUG
  printPlate(b1);
#endif
}

/**
 * Courant–Friedrichs–Lewy condition, This condition is necessary
 *
 * u is the ``thermal diffusivity'' in m^2 s^-1
 */
static inline UNIT_TEMP
cfl(UNIT_LENGTH dx, UNIT_LENGTH dy, UNIT_TIME dt, UNIT_TEMP u) {

  UNIT_TEMP courant_friedrichs_lewy =
    (u * MS_TO_S(UNIT_TEMP, dt)) / (MM_TO_M(UNIT_TEMP, thickness) * (MM_TO_M(UNIT_TEMP, dx) + MM_TO_M(UNIT_TEMP, dy)));
  assert(courant_friedrichs_lewy);
  return courant_friedrichs_lewy;
}

int
main(int argc, char ** argv) {

  UNIT_LENGTH
    w = 100,
    h = 100;

  UNIT_TEMP
    dT = 100;

  UNIT_TIME
    dt      = 1,
    simtime = 2 * 60 * 1000;

  UNIT_TEMP
    k = 1.11e-4;

  /**
   * Test stability of this setup
   */

  UNIT_TEMP stability = cfl(mesh, mesh, dt, k);

  if (stability <= 1) {
    simulate(w, h, dT, dt, simtime, k);
    return EXIT_SUCCESS;
  } else {
    snprintf(errmsg, 256, "Courant-Friedrichs-Lewy condition is not met,"
                    " this setup is instable: %.2f > 1", stability);
    perror(errmsg);
    return EXIT_FAILURE;
  }
}

