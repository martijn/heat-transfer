{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE CPP               #-}

module Main where

import           Control.Monad.State
import           Data.Array.IO
import           Data.Maybe
import qualified Data.Array as IA
import           System.IO.Unsafe
import qualified Data.List  as L
import           Text.Printf

#ifndef NDEBUG
import           Control.Concurrent
#endif

type Temperature = Double
type Time        = Int
type Length      = Int
type Coordinate  = (Length, Length)

data Plate a = Plate
  { width  :: Length
  , height :: Length
  , mesh   :: IOUArray Int a
  , tmp    :: IOUArray Int a
  }

-- Some constants
ambientT, hotT, c :: Temperature
ambientT = 293.15
hotT     = ambientT + 100
c        = 1.1e-4 -- copper

-- Time in ms
dt, steps :: Time
dt = 1
steps = dt * 1000 * 60 * 2

-- Width, height, thickness and mesh resolution in mm
w_, h_, d_, res :: Length
w_   = 100
h_   = 100
d_   = 1
res  = 1

stepscale :: Temperature
stepscale =
  let dt_s  = fromIntegral dt  / 1000
      dd_m  = fromIntegral d_  / 1000
      res_m = fromIntegral res / 1000
   in (c * dt_s) / (dd_m * res_m)

-- Stability criterion.
cfl :: Double
cfl =
  let dt_s  = fromIntegral dt  / 1000
      dd_m  = fromIntegral d_  / 1000
      res_m = fromIntegral res / 1000
   in (c * dt_s) / (dd_m * (res_m + res_m))

#ifndef NDEBUG
sleep :: Float -> IO ()
sleep f = threadDelay . round $ 1e6 * f
#endif

main :: IO ()
main
  | cfl > 1   = error ("This simulation setup is instable: " ++ show cfl)
  | otherwise =
    do
    b  <- plate w_ h_
    b' <- simulation steps b
#ifndef NDEBUG
    print b'
#else
    return ()
#endif

-- Initialize a plate
plate
  :: Length -- Width
  -> Length -- Height
  -> IO (Plate Temperature)
plate w h =
  let row = hotT : replicate (w_ - 1) ambientT
      tab = concat (replicate h_ row)
  in do
    xs <- newListArray (0, w * h - 1) tab
    ys <- newListArray (0, w * h - 1) tab
    return (Plate w h xs ys)



{-# INLINE inBounds #-}
-- | Test if a coordinate is in the bounds of the plate
inBounds :: Plate a -> Coordinate -> Bool
inBounds b (x, y) = 0 <= x && x < width  b
                 && 0 <= y && y < height b

{-# INLINE isBoundary #-}
isBoundary :: Plate a -> Coordinate -> Bool
isBoundary b (x, _) = x == 0 || x == width b - 1

-- Strided access into the array.
ix :: Plate a -> Coordinate -> Maybe Int
ix b (x, y) | inBounds b (x, y) = Just (x + y * width b)
            | otherwise         = Nothing

unsafeIx :: Plate a -> Coordinate -> Int
unsafeIx b = fromJust . ix b

temperature :: Plate Temperature -> Coordinate -> IO Temperature
temperature b (x, y) =
  case ix b (x, y) of
    Just i  -> readArray (mesh b) i
    Nothing -> return $ if x < 0
                        then hotT
                        else ambientT

-- a b c
-- d e f
-- g h i
--
-- d - e + f - e  = d + f - 2 * e
-- b - e + h - e  = b + h - 2 * e
dT :: Plate Temperature -> Coordinate -> IO Temperature
dT b (x, y) =
  do
    middle <- temperature b (x, y)
    dUdX   <- do l <- temperature b (x-1, y)
                 r <- temperature b (x+1, y)
                 return (l + r - 2 * middle)

    dUdY   <- do u <- temperature b (x, y-1)
                 d <- temperature b (x, y+1)
                 return (u + d - 2 * middle)
    return $ stepscale * (dUdX + dUdY)

-- | Single step in the simulation. We first write all the results from the mesh
-- to a temporary array and then swap them
step :: Plate Temperature -> IO (Plate Temperature)
step b =
  let ixs = [ (x, y) | x <- [0..width b - 1], y <- [0..height b - 1]]
  in do
    forM_ ixs $ \i ->
      unless (isBoundary b i) $ do
        t    <- temperature b i
        diff <- dT   b i
        -- Write the result to tmp
        let Just i' = ix b i
        writeArray (tmp b) i' (t + diff)
    -- Return the new record with tmp and mesh swapped
    return (Plate (width b) (height b) (tmp b) (mesh b))

-- | Runs the simulation for @n@ iterations.
simulation :: Int -> Plate Temperature -> IO (Plate Temperature)
simulation 0 b = return b
simulation n b =
  do
#ifndef NDEBUG
    print b
    sleep 0.1
#endif
    b' <- step b
    simulation (n - 1) b'

instance Show (Plate Temperature) where
  show b =
    let arr       = unsafePerformIO (freeze (mesh b))
        oneLine y = L.intercalate "\t"
                      [printf "%.2f" (subtract 273.15 $ arr IA.! (i + width b * y)) | i <- [0..width b-1]]
    in unlines (fmap oneLine [0 .. height b - 1])
